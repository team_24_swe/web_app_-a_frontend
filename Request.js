function sendHttpRequest()
{
    var fName = document.getElementById("firstName").value;
    var lName = document.getElementById("lastName").value;
    var addressLine = document.getElementById("addressLine").value;
    var city = document.getElementById("city").value;
    var state = document.getElementById("state").value;
    var zipCode = document.getElementById("zipcode").value;
    
    var mapToSend = {
        "firstName" : fName,
        "lastName" : lName,
        "address" : addressLine,
        "city" : city,
        "state" : state,
        "zipcode" : zipCode
    }
    
    var jsonString = JSON.stringify(mapToSend);
    
    var xHttpReq = new XMLHttpRequest();
    
    xHttpReq.open("POST", "https://team24.demand.softwareengineeringii.com/demand", false);
    
    xHttpReq.onreadystatechange() = function()
    {
        if(xHttpReq.readyState == 4 && xHttpReq.status == 200)
        {
            console.log(xHttpReq.responseText);
        }
        else
        {
            console.log(xHttpReq.responseText);
        }
    }
    
    console.log("Testing Request");
    xHttpReq.send();